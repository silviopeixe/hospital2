import React, { Fragment, useState, useEffect } from 'react';
import { HiOutlineLocationMarker} from "react-icons/hi";
import axios from 'axios';

import "./App.css";

function App() {
  	
const [location, setLocation] = useState(false);
const [weather, setWeather] = useState(false);

//function api openweathermap

setTimeout(function(){
  window.location.reload(false);
}, 500000);


let getWeather = async (lat, long) => {
  let res = await axios.get("https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/weather", {
    params: {
      lat: lat,
      lon: long,
      appid: process.env.REACT_APP_OPEN_WHEATHER_KEY,
      lang: 'pt',
      units: 'metric'
    }


  });
  setWeather(res.data);
}


//useEffect -> realiza uma unica
useEffect(()=> {
  navigator.geolocation.getCurrentPosition((position)=> {
    getWeather(position.coords.latitude, position.coords.longitude);
    setLocation(true)
    
  })
}, [])

if (location === false) {
  return (
    <Fragment>
      Você precisa habilitar a localização no browser o/
    </Fragment>
  )
} else if (weather === false) {
  return (
    <Fragment>
    </Fragment>
  )
} else {
  
  return (
   
    <Fragment>
      <div className="container">
        <center className="titulo"><HiOutlineLocationMarker/> {weather['name']}</center>
        <center className="celcius">{weather['main']['temp'].toFixed(0)}°C</center>
        <center> <img className='icones' src={"http://openweathermap.org/img/wn/" + weather['weather'][0]['icon'] + "@2x.png"} alt="Alguma coisa"/> </center>
          {/* <h3><center>{weather['weather'][0]['description']}</center></h3> */}
            <p className='container-topicos'> Máxima: {weather['main']['temp_max'].toFixed(0)}°C</p>
            <p className='container-topicos'> Minima: {weather['main']['temp_min'].toFixed(0)}°C</p>
            {/*<li>Pressão: {weather['main']['pressure']} hpa</li> */}
            <p className='container-topicos'>Umidade: {weather['main']['humidity'].toFixed(0)}%</p>
        </div>
    </Fragment>

    
  );
}
}
export default App;
